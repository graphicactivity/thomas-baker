<?php

return array(
    '*' => array(
        'siteUrl' => null,
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
        'securityKey' => getenv('SECURITY_KEY'),
    ),

    'thomasbaker.local' => array(
        'devMode' => true,
        'allowUpdates' => true,
        'environmentVariables' => array(
            'basePath' => '/Users/GraphicActivity/Sites/thomasbaker/web/',
            'baseUrl'  => 'http://thomasbaker.local/',
        )
    ),

    'thomasbaker.co.nz' => array(
        'devMode' => false,
        'allowUpdates' => false,
        'environmentVariables' => array(
            'basePath' => '/var/www/web/',
            'baseUrl'  => 'https://harvested.co.nz/',
        )
    )
);
